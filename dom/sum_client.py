# -*- coding: utf-8 -*-

import socket


# Tworzenie gniazda TCP/IP
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_IP)

try:
    # Połączenie z gniazdem nasłuchującego serwera
    server_address = ('194.29.175.240', 31112)
    client_socket.connect(server_address)

    # Pobranie danych od użytkownika
    number1 = raw_input(u'Podaj pierwszą liczbę: ')
    number2 = raw_input(u'Podaj drugą liczbę: ')

    # Sprawdzenie czy dane nie są puste
    if number1 and number2:
        # Wysłanie danych
        client_socket.send(number1)
        client_socket.send(number2)

        # Odebranie wyniku
        response = client_socket.recv(1024)
    else:
        response = u'Nie wprowadzono danych!'

    # Wypisanie wyniku
    print 'Suma: ' + response.decode('utf-8')
except socket.error:
    print u'Brak połączenia!'
finally:
    # Zamknięcie połączenia
    client_socket.close()
