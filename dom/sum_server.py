# -*- coding: utf-8 -*-

import socket
from time import strftime


# Tworzenie gniazda TCP/IP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_IP)

# Powiązanie gniazda z adresem
server_address = ('194.29.175.240', 31112)
server_socket.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
server_socket.listen(1)
print(u'Serwer nasłuchuje połączenia')

while True:
    # Czekanie na połączenie
    connection, client_address = server_socket.accept()

    try:
        # Odebranie danych
        number1 = connection.recv(1024)
        number2 = connection.recv(1024)

        # Przetwarzanie odebranych danych
        try:
            sum = str(float(number1) + float(number2))
        except:
            sum = 'Wprowadzono błędne dane!'

        # Wysłanie wyniku
        connection.send(sum)
        print strftime("%Y-%m-%d %H:%M:%S") + ' - Wynik: ' + sum.decode('utf-8')
    finally:
        # Zamknięcie połączenia
        connection.close()
