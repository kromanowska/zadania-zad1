# -*- coding: utf-8 -*-

import unittest
import socket


class SumTests(unittest.TestCase):
    # Tworzenie socketu
    def create_socket(self):
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_IP)
        server_address = ('194.29.175.240', 31112)
        client_socket.connect(server_address)
        return client_socket

    # Testowanie poprawności obliczeń
    def test_sum1(self):
        client_socket = SumTests.create_socket(self)
        try:
            client_socket.sendall(str(5))
            client_socket.sendall(str(6))
            self.assertEqual(str(11.0), client_socket.recv(1024).decode('utf-8'))
        finally:
            client_socket.close()

    # Testowanie zwrotu komunikatu o błędzie - litery zamiast liczb całkowitych
    def test_sum2(self):
        client_socket = SumTests.create_socket(self)
        try:
            client_socket.sendall('a')
            client_socket.sendall('ć')
            self.assertEqual(u'Wprowadzono błędne dane!', client_socket.recv(1024).decode('utf-8'))
        finally:
            client_socket.close()

    # Testowanie zwrotu komunikatu o błędzie - None zamiast liczb całkowitych
    def test_sum3(self):
        client_socket = SumTests.create_socket(self)
        try:
            client_socket.sendall(str(None))
            client_socket.sendall(str(None))
            self.assertEqual(u'Wprowadzono błędne dane!', client_socket.recv(1024).decode('utf-8'))
        finally:
            client_socket.close()

if __name__ == '__main__':
    unittest.main()
